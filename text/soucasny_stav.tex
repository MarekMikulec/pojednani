\chapter{Současný stav poznání}
Kapitola popisuje aktuální stav poznání v~problematice prodromální diagnózy onemocnění s~Lewyho tělísky založené na aktigrafii.
Kapitola nejprve přibližuje samotné onemocnění s~Lewyho tělísky a související poruchy v~REM spánku. 
Následně pak referuje o~aktuálních pracích a jejích výsledcích v~dané oblasti.

\section{Onemocnění s~Lewyho tělísky}
\acl*{LBD} (\acs{LBD}) je skupina neorodegenerativních onemocnění která byla blíže popsána v~úvodu pojednání.
Jak bylo zmíněno, jedná se o~onemocnění, které se částí klinických příznaků překrývá s~jinými neurodegenerativními onemocněními,
například s~Alzheimerovou chorobou \cite{DLB_AD_PD, DLB}.
Není tedy triviální tyto nemoci rozlišit. V~některých vědeckých pracích, například v~\cite{ND_ML} se používá kontinuální model,
kdy jednotlivá onemocnění volně přechází v~jiná na základě sdílených klinických příznaků, viz obr.\,\ref{img:nemoci} 
volně převzatý z~\cite{ND_ML}.

\begin{figure}[!h]
    \begin{center}
        \includegraphics[width=1\linewidth]{obrazky/Nemoci.png}
    \end{center}
    \caption{Příznaky neurodegenerativních onemocnění, kontinuální model}
    \label{img:nemoci}
\end{figure}

Další komplikací je fakt, že nemoci se velmi často vyskytují v~kombinaci a jednoduchou diagnózu nelze určit.
Proto je třeba každého pacienta individuálně vyšetřit a stanovit cílenou léčbu \cite{LBD_or_DLB}. 
Z~obr.\,\ref{img:nemoci} rovněž vyplývá, že k~úspěšné identifikaci \acs*{LBD} z~pohledu analýzy poruchy spánku je třeba identifikovat
pacienty s~poruchami v~REM spánku, zároveň je třeba oddělit tyto pacienty od pacientů s~multisystémovou atrofií.
Další výzvou je rozlišení poruch v~REM spánku od poruch v~cirkadiálním rytmu pouze na základě aktigrafie. 
Poruchy v~cirkadiálním rytmu se rovněž minoritně objevují u~pacientů s~onemocněním s~Lewyho tělísky, 
ale převážně se objevují u~pacientů s~Alzheimerovou chorobou.
Syndrom neklidných nohou není možné v~rámci disertační práce identifikovat, neboť aktigraf byl při sběru dat
nošen na zápěstí nedominantní ruky, a relevantní data tedy nejsou k~dispozici.

\section{Porucha chování v~REM spánku}
\acl{RBD} (\acs{RBD}) je porucha spánku, která může předcházet neurodegenerativním onemocněním,
například Parkinsonově chorobě, nebo demenci s~Lewyho tělísky, viz obr.\,\ref{img:nemoci}.
Identifikace \acs{RBD} výrazně pomáhá s~určením diagnózy demence s~Lewyho tělísky, 
v~kombinaci s~dalšími klinickými parametry lze dosáhnout 90\% senzitivity a 73\% specificity diagnostiky 
tohoto onemocnění v~prodromálním stádiu \cite{RBD_PS}. 
Zároveň je třeba zmínit, že se porucha chování v~REM spánku vyskytuje u~40\% pacientů 3 roky před diagnózou \acs*{RBD}
a u~66\% pacientů v~době stanovení diagnózy \acs*{RBD} \cite{PRE_LBD}, 
v~příslušné studii ovšem probíhala diagnóza \acs*{RBD} pouze na základě klinického dotazníku NACC-USD verze 3 \cite{NACC-UDSv3}.
Je zřejmé, že pacienti s~poruchou v~REM spánku jsou podmnožinou pacientů s~\acs*{DLB} v~prodromální fázi, 
a zároveň se může jednat o~pacienty s~Parkinsonovou chorobou či multisystémovou atrofií. 

\section{Automatická identifikace spánku}
\label{TSleepWake}
K~tomu, aby mohla být diagnostikována porucha chování v~REM spánku a následně konkrétní neurodegenerativní 
onemocnění z~kategorie onemocnění s~Lewyho tělísky, je třeba podstoupit vyšetření spánku.
To obnáší použití účinné metody, která identifikuje spánkové parametry a umožní rozlišit spánek
zdravého jedince od pacienta s~\acs{LBD} v~prodromální fázi.

\subsection{Polysomnografie}
V~rámci polysomnografie je vstupem celá řada specializovaných přístrojů, které umožňují podrobně klasifikovat
a rozdělit spánek na spánkové fáze.
Hypnogram je výsledkem vyšetření polysomnografie, kdy jsou jednotlivé fáze graficky znázorněny 
v~závislosti na čase.
Příklad hypnogramu z~vyšetření polysomnografie je znázorněn na obr.\,\ref{img:hypnogram}.

\begin{figure}[!h]
    \begin{center}
        \includegraphics[width=0.8\linewidth]{obrazky/hypnogram.png}
    \end{center}
    \caption{Hypnogram vytvořený na základě polysomnografického vyšetření}
    \label{img:hypnogram}
\end{figure}

\noindent Z~obrázku je patrné, že na základě polysomnografie je možné spánek rozdělit do těchto fází:
\begin{enumerate}
    \item \textbf{bdění}: člověk je vzhůru, logicky každý hypnogram začíná a končí fází bdělosti,
    \item \textbf{REM spánek}: fáze spánku, při které se zdají sny a je doprovázena rychlými pohyby očí,
          právě v~této fázi spánku se vyskytuje \acl{RBD} (\acs{RBD}),
          kdy pacient provádí rychlé pohyby v~reakci na obsah snů \cite{RBDAct,RBD_ACC_SD},
    \item \textbf{NREM spánek (fáze 1 až 4)}: také značený nonREM, fáze hlubokého spánku kdy dochází
          k~regeneraci organizmu a utlumení mozkové aktivity \cite{Hypnogram}.
\end{enumerate}
Polysomnografie je ovšem drahé vyšetření náročné na specializované přístroje a kvalifikovaný
personál, a není proto možné tuto metodu používat preventivně k~včasnému odhalení
onemocnění s~Lewyho tělísky \cite{Prazaci}.


\subsection{Aktigrafie}
V~rámci aktigrafie jsou možnosti identifikace fází spánku daleko více omezené.
V~principu téměř všechny metody umožňují na základě aktigrafie stanovit, zda subjekt spí či bdí, 
nejsou ale schopny rozlišit fáze spánku.

Většina algoritmů využívá výpočet aktivity za určitý časový interval a na základě limitní hodnoty pak 
klasifikuje interval jako spánek či bdění \cite{SleepWake}. 
Velmi často se využívají poměrně zastaralé metody: 
Sadeho metoda z~roku 1989 a Cole \& Kripkeho metoda z~roku 1992 \cite{Sadeh,Cole}.
Na těchto metodách je pak založena celá skupina dalších algoritmů využívajících modelu lineární regrese,
které se často využívají v~komerčních nositelných zařízeních a aktigrafických softwarových knihovnách \cite{SleepWake}.

Jedním z~novějších algoritmů pro automatickou detekci spánku a probuzení z~aktigrafie je heuristická metoda
využívající úhel $z$ aktigrafu narozdíl od obvykle používané velikosti vektoru zrychlení \cite{Hees}.
Tato metoda umožňuje na základě délky časového okna a hraniční hodnoty úhlu přizpůsobit svoji 
specificitu a senzitivitu. V~doporučené konfiguraci 5stupňové hraniční hodnoty úhlu a 5minutového časového okna
dosahuje v~porovnání s~polysomnografií $83\pm8$\,\% přesnosti, $91\pm13$\,\% senzitivity a $45\pm23$\,\% specificity \cite{Hees}.

Jedna z~nedávno představených metod založená na parametrizaci aktigrafického signálu
a Skrytém Markovovém modelu dosahuje 72\,\% přesnosti, 84\,\% senzitivity a 63\,\% specificity \cite{SleepWake}.

Zajímavý způsob zpracování aktigrafických dat je použit ve studii systému RBDAct \cite{RBDAct}.
Ten se nezaměřuje primárně na klasifikaci spánku a probuzení, ale s~vizí dalšího kroku zpracování dat a 
snahy odhalit poruchu chování v~REM spánku analyzuje především parametry pohybu.
Rovněž se snaží zaměřit se pouze na fáze spánku, u~kterých předpokládá, že se jedná o~REM spánek \cite{RBDAct}.
S~ohledem na další výsledky studie se jedná o~slibný a velmi netradiční postup.

O~kompletnější sestavení hypnogramu, kde by byly rozlišeny fáze spánku REM, NREM a bdění, se pokouší studie \cite{Hypnogram}.
Zde je však mimo aktigrafii využit také elektrokardiogram (\acs{ECG}) a měření respirační činnosti.
S~těmito daty bylo dosaženo 78\,\% přesnosti identifikace NREM, REM a bdění \cite{Hypnogram}.

\section{Aktuální výzkum v~oblasti identifikace poruch chování v~REM spánku}
\label{TRBD}
Jelikož většina metod založených na aktigrafii neumožňuje identifikaci \acs{REM} spánku, 
komplikuje se možnost identifikace poruch chování v~REM spánku (\acs{RBD}), neboť se nelze zaměřit
specificky na data z~REM fáze spánku.
Významné množství metod identifikace \acs{RBD} tak souvisí s~jistou chybou měření danou povahou aktigrafie, 
kdy například prudké pohyby v~REM spánku jsou algoritmem vyhodnoceny jako probuzení. 
To způsobí vysokou míru fragmentace spánku a ta je teprve detekována jako parametr klíčový 
pro identifikaci \acs{RBD}, čehož si všímá například studie \cite{WithoutPD}.

Na identifikaci dalších biomarkerů \acs{RBD}, které by odlišily pacienty od zdravých kontrol, se zaměřuje článek \cite{Prazaci}.
Práce identifikovala jako nejvýznamnější biomarker parametr \acl*{SIB} (\acs*{SIB}).
Práce ovšem vychází z~proprietárního software CamNTech dodávaného společně s~aktigrafy MotionWare.
Jelikož není nikde uveden přesný postup výpočtu parametru a zdrojové kódy nejsou veřejné, není biomarker lehce přenositelný.
Malá neočekávaná změna softwaru CamNTech může výsledky plně zneplatnit. Totéž platí pro ostatní biomarkery z~tohoto článku.
Práce rovněž používá \acl{RBD-SQ} (\acs{RBD-SQ}) jako další zdroj dat. 
Na základě dotazníku \acs*{RBD-SQ} v~kombinaci s~navrženým biomarkerem \acs*{SIB} studie dosáhla 87,6\% přesnosti \cite{Prazaci}. 

Další studie pracovala se vzorkem 26 lidí, kteří absolvovali 2 návštěvy spánkové laboratoře (zaznamenána data aktigrafie a polysomnografie)
a dvoutýdenní záznam aktigrafie z~domácího prostředí. Cílem práce byla klasifikaci zdravých kontrol a pacientů s~\acs{RBD} 
pomocí algoritmů strojového učení \cite{RBDAct}. 
Výsledky jsou působivé, bylo dosaženo $92\pm8$\,\% přesnosti, $94\pm7$\,\% senzitivity a $92\pm13$\,\% specificity
v~prostředí laboratoře, v~domácím prostředí pak bylo údajně dosaženo 100\,\% přesnosti \cite{RBDAct}.

S~použitím aktigrafie na vzorku 41 \acs{RBD} pacientů a 42 zdravých kontrol dokázala jiná studie
dosáhnout 94\,\% přesnosti a 95\,\% senzitivity a 93\,\% specificity při klasifikaci subjektu 
(88\,\% přesnost, 89\,\% senzitivita a 86\,\% specificita pro jednu noc) \cite{RBD_ACC_SD}.
V~kombinaci se spánkovým dotazníkem studie dosáhla 94\,\% přesnosti, 88\,\% senzitivity a 100\,\% specificity \cite{RBD_ACC_SD}.


\section{Aktuální výzkum v~oblasti identifikace onemocnění s~Lewyho tělísky na základě poruch chování v~REM spánku}
\label{TLBD}
Jedna z~prvních studií, která byla v~této oblasti provedena, se zasadila o~přidání \acs{RBD} jako 
diagnostického kritéria demence s~Lewyho tělísky.
Přídáním \acs{RBD} jako hodnotícího kritéria k~původním třem hodnotícím příznakům \acs{DLB} vzrostla senzitivita z~85\,\% na 90\,\%, 
specificita zůstala nezměněna na 73\,\% \cite{RBD_PS}.

Poruchy spánku pacientů, kteří nejsou prozatím diagnostikováni jako pacienti s~Parkinsonovou chorobou, zkoumá práce \cite{WithoutPD}.
Důležitým závěrem studie je fakt, že vyšší fragmentace spánku koreluje s~výskytem onemocnění s~Lewyho tělísky,
studie navíc uvádí, že s~rostoucí mírou fragmentace rostla i závažnost onemocnění.
V~případě že směrodatná odchylka fragmentace spánku vzroste o~1, vzroste pravděpodobnost onemocnění 
s~Lewyho tělísky o~40\% \cite{WithoutPD}.

Další studie se zaměřila na pacienty s~\acs{RBD} a pokusila se pomocí metod strojového učení (střídavý rozhodovací strom) 
rozlišit mezi pacienty, u~kterých se nemoc rozvinula v~\acs{DLB} a pacienty s~pozdější \acs{PD} \cite{PD_or_DLB}.
Experiment nebyl úspěšný, na vzorku 76 účastníků studie se nepovedlo odhalit žádný signifikantní rozdíl mezi 
pacienty s~\acs{DLB}, \acs{PD} a zdravými kontrolami. 
Studie ovšem uvádí, že je možné pomocí metod strojového učení identifikovat pacienty s~\acs{RBD} v~průměru 3,6 roku před tím,
než je jim stanovena klinická diagnóza demence \cite{PD_or_DLB}.



\chapter{Stav řešení}
Tato kapitola si klade za cíl popsat a přiblížit provedené experimenty a úspěšná řešení,
která povedou k~systematickému řešení stanovených cílů.
Kapitola zároveň popisuje vize, které by měly k~danému cíli vést a vyhodnocuje,
jaká část cíle již byla dosažena.

\section{Automatická identifikace spánku z~dat aktigrafie}
Současné řešení umožňuje automatickou identifikaci spánku z~dat aktigrafie a je schopné
klasifikovat časové úseky, ve kterých subjekt spal či bděl.
Není ale možné stanovit fáze spánku, tedy identifikovat REM fázi spánku.
V~navazujících podkapitolách bude obsažen výčet jednotlivých implementovaných metod a jejich porovnání.

Metody byly testovány a trénovány s~pomocí veřejně dostupného datasetu
\textit{Newcastle polysomnography and accelerometer data}
\cite{Dataset}.
Tento dataset obsahuje 55 aktigrafických souborů od 28 různých subjektů
z~aktigrafu GENEActiv (v~jednu chvíli měl subjekt dva aktigrafy, na pravé a levé ruce, jeden záznam z~aktigrafu byl poškozen).
Dále pak dataset obsahuje odpovídajících 28 měření polysomnografie ze stejné noci,
která byla použita jako zdroj absolutní pravdy pro trénování klasifikátorů s~učitelem.

K~porovnání metod s~parametry ze spánkových deníků byl použit dataset vytvořený ve spolupráci s~neurology
z~Fakultní nemocnice u~sv. Anny, dále pak dataset vytvořený ve spolupráci s~Fakultní nemocnicí v~Olomouci,
a rovněž data získaná v~rámci evropského projektu CE1581.
Dataset obsahuje celkem 137 různých subjektů a 667 nocí, kdy jsou k~dispozici data z~aktigrafu GENEActiv a odpovídající
spánkové deníky.

\subsection{Metoda XGBoost + velikosti zrychlení + teplota XMT}
\label{XMT}
Tato metoda byla implementována v~rámci diplomové práce \cite{Diplomka}.
Následně pak byla vylepšena a prezentována na studentské konferenci EEICT 2021 a mezinárodní konferenci TSP 2021 \cite{EEICT2021,TSP2021}.
Metoda bude dále značená \acs{XMT} (XGBoost Magnitude Temperature).

XGBoost je metoda strojového učení založená na principu Tree boosting.
Metoda je hojně využívaná v~celé řadě úspěšných studií.
V~prestižních soutěžích pořádaných například stránkou
\url{https://www.kaggle.com}, se řešení využívající
me\-to\-du XGBoost pravidelně umisťují na předních příčkách.
Zároveň je možné díky škálovatelnosti a dobré optimalizaci použít metodu XGBoost na
nespecializovaném hardware a dosáhnout výsledků v~relativně krátkém čase \cite{XGBoost}.

Metoda používá data z~aktigrafu GENEActiv, stejně jako metody následující.
Aktigraf produkuje časové řady se záznamy v~následujícím formátu:
\begin{itemize}
    \item časové razítko ve formátu \%Y-\%m-\%d \%H:\%M:\%S:\%L,
    \item zrychlení v~ose X,
    \item zrychlení v~ose Y,
    \item zrychlení v~ose Z,
    \item úroveň osvětlení,
    \item stav tlačítka,
    \item teplota.
\end{itemize}
V~rámci této metody nebyla využita informace o~úrovni okolního osvětlení a stav tlačítka.
Určení časového okna, ve kterém má být analyzován spánek, probíhá na základě záznamu subjektu
do spánkového deníku, nebo je použita celá časová řada.
Posloupnost zpracování dat bude popsána v~následujících krocích.

\subsubsection{Parametrizace}
\begin{enumerate}
    \item Rozdělení časového okna na 30\,s intervaly. Rozdělení na tyto intervaly odpovídá standardu pro
          polysomnografii.
    \item Výpočet velikosti vektoru $m$ na základě zrychlení v~ose $x$, $y$ a $z$ podle vzorce
          \begin{equation} \label{Magnitude}
              m = \sqrt{x^2 + y^2 + z^2},
          \end{equation}
          který umožňuje vyjádřit zrychlení v~osách $x$, $y$ a $z$ pomocí jediné veličiny velikosti vektoru.
          Zároveň ovšem dochází ke ztrátě některých informací, například informace o~směru pohybu.
    \item Pro 30\,s vektor $m$ a 30\,s vektor teploty je stanoveno 45 statistických parametrů, dohromady tedy 90 parametrů.
          Parametry jsou vyjmenovány v~tabulce \ref{tab:features}.
    \item Přiřazení odpovídající hodnoty z~polysomnografie v~případě, že je k~dispozici.
          Polysomnografie slouží v~procesu učení s~učitelem při trénování a testování modelu jako absolutní pravda.
\end{enumerate}

\input{text/Features.tex}

\subsubsection{Klasifikace}
Ke klasifikaci dat byl použit model XGBoost natrénovaný s~pomocí datasetu \cite{Dataset}.
Podrobný popis přípravy modelu lze nalézt v~diplomové práci \cite{Diplomka}, pro potřeby tohoto textu uvádíme stručný postup:
\begin{itemize}
    \item rozdělení datasetu na trénovací a testovací data v~poměru 60\,\% ku 40\,\%,
    \item hledání nejlepších hyperparametrů pro binární klasifikátor XGBoost,
    \item kontrola volby hyperparametrů pomocí křížové validace,
    \item natrénování modelu s~nejlepšími hyperparametry,
    \item otestování modelu na testovacích datech.
\end{itemize}

\subsubsection{Výsledky a interpretace}
Model dosáhl na testovacích datech 80\,\% přesnosti, 92\,\% senzitivity a 55\,\% specificity \cite{Diplomka}.
Model si ovšem zvolil jako nejdůležitější parametr pátý percentil teploty.
To je vhodná metrika pro trénovací dataset, který byl pořízen v~laboratorních podmínkách se stálou teplotou,
na datech pořízených při domácím měření se ale ukázala nevhodnost této volby parametru.
Při srovnání se spánkovými deníky algoritmus dosáhl
67\,\% přesnosti, 73\,\% senzitivity a 23\,\% specificity \cite{TSP2021}.

\subsection{Metoda Van Hees AZ}
\label{AZ}
Další otestovanou metodou byla metoda doporučená Van Hees et al. \cite{Hees},
metoda bude dále značena AZ (Angle $z$).
Metoda využívá pouze zrychlení v~ose $x$, $y$ a $z$, na rozdíl od většiny metod je ovšem
nezpracovává pomocí velikosti vektoru, ale pomocí úhlu $z$ ($\angle z$).
Ten se spočítá na základě následujícího vztahu:
\begin{equation} \label{ArmAngle}
    \angle z~= \left(\mbox{tan}^{-1}\frac{a_z}{\sqrt{a_x ^2 + a_y ^2}}\right)\cdot \frac{180}{\pi},
\end{equation}
kde $a_x$, $a_y$ a $a_z$ jsou mediány klouzavého 5minutového průměru zrychlení v~osách $x$, $y$ a $z$.
Klasifikace spánku/bdění pak probíhá jednoduše, pokud změna $\angle z$ v~5 minutách překročí hodnotu 10\textdegree,
pak je interval klasifikován jako probuzení, jinak je klasifikován jako spánek.

Metoda dosáhla 75\,\% přesnosti, 76\,\% senzitivity a 70\,\% specificity \cite{TSP2021}.
Tyto výsledky se zásadně odlišovaly od výsledků prezentovaných v~původní studii \cite{Hees}.
V~porovnání se spánkovými deníky pak metoda dosáhla 67\,\% přesnosti, 71\,\% senzitivity a 47\,\% specificity.

\subsection{Metoda XGBoost + velikosti zrychlení + teplota + úhel $z$ XMTAZ}
\label{XMTAZ}
Nově implementovanou metodou, která prozatím nebyla publikována, je kombinace předchozích dvou metod.
Vychází z~metody XMT, odlišnost je v~parametrizaci.
Zde se v~původním kroku 2 vypočítá navíc ještě $\angle z$ obdobně jako v~metodě AZ,
vztah je ovšem pozměněný následujícím způsobem:
\begin{equation} \label{ArmAngleActual}
    \angle z~= \left(\mbox{tan}^{-1}\frac{z}{\sqrt{x ^2 + y ^2}}\right)\cdot \frac{180}{\pi},
\end{equation}
kde $x$, $y$ a $z$ jsou aktuální hodnoty zrychlení v~příslušných osách a nikoliv klouzavé průměry
jako v~případě vztahu \ref{ArmAngle}.
30\,s vektor aktuálních hodnot $\angle z$ je následně parametrizován stejně jako vektor $m$ a
společně s~tímto vektorem a teplotami je pro každý 30\,s interval spočítáno 135 parametrů.
Další postup je shodný s~\acs{XMT}.

Metoda dosáhla 83\,\% přesnosti, 92\,\% senzitivity a 62\,\% specificity.
Navíc nedošlo na testovacích datech k~signifikantnímu zhoršení a
výsledky odpovídaly výsledkům křížové validace na trénovacích datech.
To znamená, že algoritmus nebyl přetrénován.
Patrné je především zlepšení specificity, které vyplývá právě z~kombinace obou zmíněných metod.
V~porovnání se spánkovými deníky algoritmus dosáhl
76\,\% přesnosti, 82\,\% senzitivity a 31\,\% specificity, což je lepší výsledek než u~předchozích
dvou algoritmů.
Vzhledem k~tomu, že je pro subjekt poměrně náročné zaznamenat přesnou chvíli probuzení,
bylo by vhodnější místo přesné shody každého časového okamžiku identifikace spánku a probuzení
porovnat spánkové parametry jako celková doba spánku a počet probuzení.
Tuto metodu používají studie \cite{SleepWake,Hees}.

Jako nejvýznamnější parametr byl opět vybrán parametr teploty, konkrétně její desátý percentil.
Třetím v~pořadí významnosti parametrů byl mezidecilový rozsah $\angle z$.
Deset nejvýznamnějších parametrů je zobrazeno na obr.\,\ref{img:features}.
Překlady parametrů lze dohledat v~tab.\,\ref{tab:features}, přičemž TEMPERATURE jsou parametry založené
na teplotě, Z\_ANGLE parametry založené na $\angle z$ a MAGNITUDE parametry založené na velikosti vektoru $m$.
Dle samotné hodnoty významnosti parametru je zřejmá přetrvávající dominance prvních dvou parametrů
získaných z~dat teploty.
V~metodě tedy stále zůstává zachováno riziko přeučení se na datech z~laboratorních podmínek se stálou
teplotou.

\begin{figure}[!h]
    \begin{center}
        \includegraphics[width=0.65\linewidth]{obrazky/featuresV.png}
    \end{center}
    \caption{Deset nejvýznamnějších parametrů klasifikace spánku a bdění}
    \label{img:features}
\end{figure}

Model byl následně interpretován s~pomocí SHAP values, což je metoda pocházející z~teorie her,
která určuje, jak moc se podílí každý bod každého parametru na finálním
výstupu modelu \cite{SHAP}.
Zde již na prvním místě dominoval aktigrafický parametr $\angle z$, konkrétně mezidecilový rozsah.
Obr.\,\ref{img:shap} zobrazuje dvacet nejdůležitějších parametrů podle metody SHAP.

\begin{figure}[!h]
    \begin{center}
        \includegraphics[width=0.99\linewidth]{obrazky/shap.png}
    \end{center}
    \caption{Dvacet nejvýznamnějších parametrů klasifikace spánku a bdění podle metody SHAP}
    \label{img:shap}
\end{figure}
\noindent Model využívá parametry ze všech 3 vstupních vektorů dat. Měla by tedy být zajištěna 
vyšší robustnost modelu proti nečekaným změnám, například změně teploty při odkrytí a přikrytí přikrývkou.
To dokazuje i vyšší míra shody se spánkovými deníky, kdy pracujeme s~daty nasbíranými
v~proměnlivém domácím prostředí pacientů.


\subsection{Srovnání metod automatické identifikace spánku z~dat aktigrafie}
V~následující tab.\,\ref{tab:swresults} jsou porovnané navržené a otestované metody
automatické identifikace spánku z~dat aktigrafie s~dalšími dostupnými metodami
otestovanými v~rámci studie \cite{SleepWake}.
Porovnání s~těmito výsledky je třeba brát s~jistou rezervou, neboť neproběhlo na
stejném datasetu.

\begin{table}[htbp]
    \centering
    \caption{Srovnání algoritmů automatické detekce spánku}
    \begin{tabular}{lrrr|rrr}
        \hline
        \hline
                                  & \textbf{Sadeh} & \textbf{Cole} & \textbf{HMM} & \textbf{XMT} & \textbf{AZ} & \textbf{XMTAZ} \\
        \hline
        \textbf{Přesnost [\%]}    & 70             & 70            & 73           & 80           & 75          & 83             \\
        \hline
        \textbf{Senzitivita [\%]} & 95             & 98            & 83           & 92           & 76          & 92             \\
        \hline
        \textbf{Specificita [\%]} & 38             & 32            & 63           & 55           & 70          & 62             \\
        \hline
    \end{tabular}%
    \label{tab:swresults}%
\end{table}%

\noindent Jedná se o~následující modely:
\begin{enumerate}
    \item \textbf{Sadeh}: metoda založená na studii \cite{Sadeh}, lineární regrese
    \item \textbf{Cole}: metoda založená na studii \cite{Cole}, lineární regrese
    \item \textbf{HMM}: metoda založená na studii \cite{SleepWake}, skrytý markovův model
    \item \textbf{XMT}: nově navržená metoda viz \ref{XMT}, XGBoost
    \item \textbf{AZ}: metoda založená na studii \cite{Hees} viz \ref{AZ}, statistická metoda
    \item \textbf{XMTAZ}: nově navržená metoda viz \ref{XMTAZ}, XGBoost
\end{enumerate}

\subsection{Vyhodnocení splnění cíle 1}
Cíl \emph{Návrh pokročilých technik tvorby hypnogramu pomocí heuristických a statistických metod} se zdá být splněným.
Navržená metoda XMTAZ dosahuje nejlepších výsledků a překonává i nově navrženou metodu \cite{SleepWake}.
Jedná se pouze o~automatickou identifikaci spánku z~dat aktigrafie, ze současného stavu poznání a provedených
experimentů však vyplývá, že tvorba hypnogramu podobného polysomnografii nemusí být splnitelná a detekce spánku je
pro následující experimenty dostačující.

\subsection{Navazující práce}
\label{SWadditionalWork}
Ačkoliv je navržena kompetitivní metoda s~dobrými výsledky, stále je zde několik oblastí, ve kterých
by použitá metoda mohla být vylepšena.
Prvním vylepšením by měla být lepší detekce celkového okna, ve kterém identifikujeme spánek.
V~současné chvíli je prováděna s~dopomocí záznamu spánkových deníků.
Dalo by se ovšem použít sofistikovanější metodu, například identifikovat spánkové okno na
základě poklesu okolního osvětlení, jako to činí studie \cite{RBDAct}.

Další způsob vylepšení by byla filtrace dat aktigrafie, která by odstranila vliv gravitačního působení.
K~tomu se používá 0,5-11\,Hz Butterworthůw filtr \cite{Gravitace}, tuto metodu použila například studie \cite{SleepWake}
či studie \cite{Hees}.

Velmi podstatným krokem, který by měl být učiněn v~navazující disertační práci, 
je filtrace šumu způsobená občasnou špatnou klasifikací.
Například obr.\,\ref{img:acgexample} znázorňuje klasifikaci spánku, která dle deníku skončila v~6:30 a následně
subjekt zůstal v~posteli až do 7:15. Žlutou barvou je na obrázku zvýrazněn spánek.
Obrázek pochází z~aplikace \cite{nice-life} z~evropského projektu, kde byla metoda prakticky využita.
\begin{figure}[!h]
    \begin{center}
        \includegraphics[width=0.99\linewidth]{obrazky/acc_example.png}
    \end{center}
    \caption{Příklad šumu v~klasifikaci spánku}
    \label{img:acgexample}
\end{figure}

\noindent Z~obr.\,\ref{img:acgexample} je patrné, že mezi 6:30 a 7:15 byly zaznamenány krátké okamžiky spánku.
Jedná se nejspíše o~chybnou klasifikaci, kdy se model zmýlil kvůli nečinnosti subjektu.
Ačkoliv v~celkových výsledcích klasifikaci spánku neudělá takováto chybná klasifikace pár hodnot téměř žádný rozdíl,
při stanovování parametrů spánku to může být zásadní problém. Například zde by byl počátek spánku stanoven na půlnoc a konec
spánku chybně na 7:15 místo 6:30.
Výrazně by narostl například parametr celkové doby probuzení mezi usnutím a probuzením a vzrostl by výrazně i parametr
fragmentace spánku.
Je tedy třeba uplatnit nějakou metodu filtrace, například klouzavý průměr, která by tento šum eliminovala.

Rovněž by bylo možné vyzkoušet některou z~metod, která by pomohla kromě identifikace spánku a probuzení
identifikovat i fáze spánku, především pak spánek REM.
Takové metody jsou navrženy například ve studii \cite{RBDAct}, nutno podotknou že se jedná o~značně zjednodušené řešení.
Zde již bohužel narážíme na možnosti aktigrafie, která nemůže poskytnout tak kvalitní výsledky jako
polysomnografické vyšetření, neboť spousta informací z~množství polysomnografických senzorů prostě neexistuje.

\section{Identifikace poruch chování v~REM spánku}
Navržené řešení umožňuje stanovit diagnózu o~přítomnosti poruch chování v~REM spánku jak po jednotlivých
nocích, tak celkově pro konkrétní subjekt. Metoda byla představená na konferenci EEICT 2022 \cite{EEICT2022}, kde byla
metoda specificky zaměřena na klasifikaci pacientů s~Parkinsonovou chorobou v~prodromální fázi.
Metoda byla následně vylepšena použitím představené klasifikace XMTAZ a rozšířena na obecnou identifikaci
poruchy chování v~REM spánku.
Poté byla prakticky nasazena v~systému evropského projektu CE1581 \cite{nice-life}.

\subsection{Dataset}
Jako dataset pro identifikaci \acs{RBD} byla zpracována aktigrafická data a spánkové deníky od 113 subjektů pomocí
metody XMTAZ. Z~demografického pohledu se jednalo o~subjekty ve věku $64\pm 14$ let,
dataset obsahoval 54\,\% dat od žen a 46\,\% dat od mužů.
Dohromady bylo k~dispozici 667 nocí, přičemž 294 (44\,\%) bylo získáno od pacientů s~možnou poruchou v~REM spánku,
a 373 (56\,\%) bylo získáno od zdravých kontrol.
Diagnózy byly stanoveny neurology z~1. neurologické kliniky Fakultní nemocnice u~sv. Anny, dataset obsahoval
pacienty s~možnou Parkinsonovou chorobou a pacienty s~možnou mírnou kognitivní poruchou,
která může být předstupněm Alzheimerovi choroby.
Tito pacienti byli označeni jako pacienti s~poruchami v~REM spánku pro potřeby této studie, neboť na finální
diagnóze se stále pracuje na straně neurologů, zároveň se jedná o~pacienty v~případné rané fázi
onemocnění s~Lewyho tělísky, kdy ještě není zcela známá diagnóza a bude jasno až s~postupem času a rozvojem onemocnění.

\subsection{Parametrizace}
Ná základě dat z~aktigrafu zpracovaných metodou \acs{XMTAZ} a na základě dat ze spánkových deníků bylo stanoveno
26 parametrů, pro každý z~těchto zdrojů dat se jednalo o~stejných 13 parametrů.
Tab.\,\ref{tab:features2} vyjmenovává použité parametry, uvádí jejich český překlad a zkratku.
Tab.\,\ref{tab:features3} uvádí k~jednotlivým parametrům jejich popis, a dále vztah, který je použit k~jejich
výpočtu na základě jiných parametrů.
Některé parametry vychází z~norem stanových National Sleep Foundation's \cite{Norms}.
Tyto normy stanovují na základě věku 3 stavy parametru:
\begin{itemize}
    \item nevyhovující (Inappropriate) [-1],
    \item neurčitý (Uncertain) [0],
    \item vyhovující (Appropriate) [1].
\end{itemize}
Pro automatické zpracování dat jsou tyto kategorie označeny čísly -1, 0 a 1.
Z~důvodu správného zařazení do věkové kategorie je potřeba znát věk subjektu.
% Table generated by Excel2LaTeX from sheet 'Sheet1'

\begin{table}[htbp]
    \centering
    \caption{Parametry spánku a jejich překlad a zkratka}
    \resizebox{\textwidth}{!}{%
        \begin{tabular}{lll}
            \hline
            \hline
            \textbf{Parametr [EN]}        & \textbf{Parametr [CZ]}                      & \textbf{Zkratka} \\
            \hline
            Time in bed                   & Čas v~posteli                               & TIB              \\
            \hline
            Sleep onset latency           & Délka usnutí                                & SOL              \\
            \hline
            Sleep onset latency - norm    & Délka usnutí dle normy                      & SOL-N            \\
            \hline
            Wake after sleep onset        & Probuzení po usnutí                         & WASO             \\
            \hline
            Wake after sleep onset - norm & Probuzení po usnutí dle normy               & WASO-N           \\
            \hline
            Wake after sleep offset       & Délka vstávání z~postele                    & WASF             \\
            \hline
            Total sleep time              & Celková doba spánku                         & TST              \\
            \hline
            Wake bouts                    & Počet probuzení                             & WB               \\
            \hline
            Awakening > 5 minutes         & Počet probuzení delší než 5 minut           & WB5              \\
            \hline
            Awakening > 5 minutes - norm  & Počet probuzení delší než 5 minut dle normy & WB5-N            \\
            \hline
            Sleep efficiency              & Efektivita spánku                           & SE               \\
            \hline
            Sleep efficiency - norm       & Efektivita spánku dle normy                 & SE-N             \\
            \hline
            Sleep fragmentation           & Fragmentace spánku                          & SF               \\
            \hline
        \end{tabular}%
        \label{tab:features2}%
    }
\end{table}%

\begin{table}[htbp]
    \centering
    \caption{Popis parametrů spánku, postup výpočtu}
    \resizebox{\textwidth}{!}{%
        \begin{tabular}{ll}
            \hline
            \hline
            \textbf{Zkratka} & \textbf{Popis parametru}                                                                                      \\
            \hline
            TIB              & Čas v~posteli, po který se aktivně snažíte usnout                                                             \\
            \hline
            SOL              & Čas strávený snahou usnout                                                                                    \\
            \hline
            SOL-N            & SOL hodnocené dle normy                                                                                       \\
            \hline
            WASO             & Součet všech probuzení po prvním úspěšném usnutí až do finálního probuzení                                    \\
            \hline
            WASO-N           & WASO hodnocené dle normy                                                                                      \\
            \hline
            WASF             & Doba mezi finálním probuzením a opuštěním postele                                                           \\
            \hline
            TST              & Součet veškerého spánku: $\mbox{TST} = \mbox{TIB} - (\mbox{SOL} + \mbox{WASO} + \mbox{WASF})$                 \\
            \hline
            WB               & Počet probuzení během noci                                                                                    \\
            \hline
            WB5              & Počet probuzení během noci, kdy doba probuzení je delší než 5 minut                                           \\
            \hline
            WB5-N            & WB5 hodnocené dle normy                                                                                       \\
            \hline
            SE               & Celková doba spánku děleno časem stráveným v~posteli: $\mbox{SE} = \frac{\mbox{TST}}{\mbox{TIB}} \cdot 100$   \\
            \hline
            SE-N             & SE hodnocené dle normy                                                                                        \\
            \hline
            SF               & Počet probuzení děleno celková doba spánku v~hodinách: $\mbox{SF} = \frac{\mbox{WB}}{\mbox{TST} \cdot 3600} $ \\
            \hline
        \end{tabular}%
        \label{tab:features3}%
    }
\end{table}%

K~parametrům pro každou noc je přiřazena informace, zda pochází od pacienta s~předpokládanou poruchou spánku či nikoliv.
Tato hodnota je brána jako absolutní pravda pro trénování a testování modelu
(což může vnášet do řešení jistou nepřesnost, neboť diagnóza není konečná v~době psaní této práce).
Výsledný dataset je složen z~jednotlivých nocí, klasifikace se rovněž provádí noc po noci nikoliv subjekt po subjektu.

\subsection{Klasifikace}
Ke klasifikaci dat byl opět použit model XGBoost. Postup je totožný, tedy:
\begin{itemize}
    \item rozdělení datasetu na trénovací a testovací data v~poměru 60\,\% ku 40\,\%,
    \item hledání nejlepších hyperparametrů pro binární klasifikátor XGBoost,
    \item kontrola volby hyperparametrů pomocí křížové validace,
    \item natrénování modelu s~nejlepšími hyperparametry,
    \item otestování modelu na testovacích datech.
\end{itemize}
Proces je možné replikovat s~pomocí Jupiter notebooku dostupného
z~\href{https://drive.google.com/drive/folders/1mHuKMrAXn3pFc-Nshn40vHeZO9ttOZSp}{Google disku}, jedná se o~první verzi prezentovanou
v~rámci konference EEICT 2022 \cite{EEICT2022}.
Aktuální verze je k~dispozici na \cite{nice-life}, kde je začleněna do spánkového systému.

\subsection{Výsledky}
Výsledky klasifikace jednotlivých nocí nejsou na první pohled příliš působivé.
Modelu se povedlo dosáhnout 62\,\% přesnosti, 70\,\% senzitivity a 55\,\% specificity.
Je ale třeba si uvědomit, že pacient s~poruchou spánku může mít některé noci dobrý spánek,
zatímco subjekt spadající do kategorie zdravých kontrol může mít špatnou noc například
na základě stresu.
Z~toho důvodu probíhalo měření po dobu přibližně 6 až 7 dnů pro každý subjekt.
Následně se vyhodnotily výsledky klasifikace všech nocí daného subjektu. 
Pokud bylo více nocí klasifikováno jako potencionální porucha spánku, 
byl subjekt klasifikován jako pacient s~\acs{RBD}.
V~opačném případě byl klasifikován jako zdravá kontrola.
Ze 137 subjektů bylo špatně klasifikováno pouze 6 subjektů, metoda tak dosáhla
95\,\% přesnosti, 100\,\% senzitivity a 91\,\% specificity
(metrika $\mbox{F}_1$ dosáhla 94\,\%, Matthewsův korelační koeficient pak hodnoty 0,9).

\subsection{Interpretace klasifikátoru}
\label{resin}
Na obr.\,\ref{img:features2} jsou prezentovány parametry spánku seřazené dle jejich
významnosti.
Parametry pocházející z~aktigrafu jsou označeny (A), parametry ze
spánkových deníků pak (D), překlady parametrů lze nalézt v~tab.\,\ref{tab:features2},
popis parametrů v~tab.\,\ref{tab:features3}.
Poměrně zajímavý je nejvýznamnější parametr, tím je dle modelu SOL-N, tedy čas strávený snahou usnout klasifikovaný
dle normy \cite{Norms} na základě aktigrafie.
Druhým v~pořadí je pak parametr WASO-N založený na aktigrafii, tedy součet všech probuzení po prvním úspěšném usnutí
až do finálního probuzení dle normy \cite{Norms}.
Z~obr.\,\ref{img:features2} je rovněž patrné, že všech pět nejdůležitějších parametrů
je založeno na normách.

\begin{figure}[!h]
    \begin{center}
        \includegraphics[width=0.9\linewidth]{obrazky/featuresHL.png}
    \end{center}
    \caption{Spánkové parametry seřazené dle významnosti klasifikace RBD}
    \label{img:features2}
\end{figure}

Model byl opět analyzován pomocí metody SHAP popsané v~kapitole \ref{XMTAZ}.
Na obr.\,\ref{img:shap2} je zobrazeno, jak moc se podílí každý bod každého parametru na finálním
výstupu modelu. Zde jednoznačně dominuje WASO-N, tedy parametr který se umístil jako druhý
v~hodnocení významnosti parametrů.

\begin{figure}[!h]
    \begin{center}
        \includegraphics[width=0.99\linewidth]{obrazky/shapHL.png}
    \end{center}
    \caption{Parametry klasifikace spánku seřazené podle důležitosti klasifikace RBD podle metody SHAP}
    \label{img:shap2}
\end{figure}

Metoda SHAP také umožňuje zobrazit graf \texttt{beeswarm}, na kterém je teplotní mapou zobrazeno
rozložení a důležitost konkrétních bodů jednotlivých parametrů. Tento graf je zobrazen na obr.\,\ref{img:shap3}.

\begin{figure}[!h]
    \begin{center}
        \includegraphics[width=0.99\linewidth]{obrazky/beeswarm.png}
    \end{center}
    \caption{Rozložení jednotlivých bodů klasifikace spánku seřazené podle důležitosti klasifikace RBD podle metody SHAP s~teplotní mapou významnosti}
    \label{img:shap3}
\end{figure}

Z~rozložení nejvýznamnějšího parametru WASO-N je například zřejmé, že subjekty,
které mají klasifikované probuzení v~průběhu spánku jako nevyhovující či neurčité,
jsou ve většině případů klasifikovány jako pacienti s~RBD.
Naopak WASO-N vyhovující normě je charakteristické pro zdravé kontroly.
Konkrétní rozsah norem pro jednotlivé věkové kategorie je zobrazen na obr.\,\ref{img:norm}
převzatém z~doporučení \cite{Norms}. Jelikož subjekty spadají průměrným věkem někde na pomezí
dospělých a starších dospělých, u~kterých už není zastoupená hodnota parametru nevyhovující,
přesouvá se těžiště více doprostřed k~stavu neurčitý.
Norma tedy naznačuje, že noci s~délkou probuzení delší jak
půl hodiny označují noci s~poruchou spánku a
potencionální onemocnění s~Lewyho tělísky v~prodromálním stádiu.
Jedná se ovšem o~velmi zjednodušenou interpretaci 
a funkčnost biomarkeru by bylo třeba ověřit dalšími experimenty.

\begin{figure}[!h]
    \begin{center}
        \includegraphics[width=0.99\linewidth]{obrazky/waso-norm-cz.png}
    \end{center}
    \caption{Norma pro parametr WASO dle věkových skupin}
    \label{img:norm}
\end{figure}

Zajímavé je rovněž rozložení druhého nejvýznamnějšího parametru,
celkové doby spánku podle spánkového deníku.
Z~rozložení vyplývá, že příliš krátká i příliš dlouhá doba spánku indikuje
možnou poruchu spánku.
Následující parametry už se nezdají na základě rozložení jednoznačně interpretovatelné.
U~třetího parametru rozložení navíc naznačuje, že krátká doba usnutí, tedy nízká hodnota
SOL na základě aktigrafie, může značit poruchu spánku.
To spíše odporuje klinické praxi, dle norem je SOL kratší než 30 minut naopak vyhovující.
Zde se projevuje nejspíše nedokonalost identifikace počátku a konce spánku,
která byla diskutována v~kapitole \ref{SWadditionalWork}.

\subsection{Srovnání metody identifikace RBD s~existujícími metodami}
V~rámci aktuálního stavu výzkumu byly nalezeny dvě studie, které představují metody 
identifikace RBD z~dat aktigrafie \cite{RBDAct,RBD_ACC_SD}.
S~těmito metodami bude porovnána námi navržená metoda značená XGB (XGBoost),
metoda \cite{RBDAct} je značena RBDAct dle názvu studie a
metoda \cite{RBD_ACC_SD} je značena AQ (Actigraphy \& Questionnaire).
Výsledky pro jednu noc jsou zobrazeny v~tab.\,\ref{tab:rbd1} (v~případě metody
RBDAct se jedná o~výsledky dvou nocí měřených v~spánkové laboratoři).
Dle porovnání námi navržená metoda dosahuje mnohem horších výsledků.
Nejlepších výsledků dosahuje metoda RBDAct s~modelem \acl{SVM} (\acs{SVM}).

\begin{table}[htbp]
    \centering
    \caption{Klasifikace RBD, výsledky pro jednu noc}
    \begin{tabular}{lrrr}
        \hline
        \hline
                             & \textbf{XGB} & \textbf{RBDAct} & \textbf{AQ} \\
        \hline
        \textbf{Přesnost}    & 62           & 93              & 88          \\
        \hline
        \textbf{Senzitivita} & 70           & 95              & 89          \\
        \hline
        \textbf{Specificita} & 55           & 93              & 86          \\
        \hline
    \end{tabular}%
    \label{tab:rbd1}%
\end{table}%

\noindent Tab.\,\ref{tab:rbd2} zobrazuje výsledky nejlepší konfigurace všech metod, kdy jsou klasifikovány
přímo subjekty studií na základě více dostupných nocí.

\begin{table}[htbp]
    \centering
    \caption{Klasifikace RBD, výsledky pro subjekty (víc nocí)}
    \begin{tabular}{lrrr}
        \hline
        \hline
                             & \textbf{XGB} & \textbf{RBDAct} & \textbf{AQ} \\
        \hline
        \textbf{Přesnost}    & 95           & 100             & 94          \\
        \hline
        \textbf{Senzitivita} & 100          & 100             & 88          \\
        \hline
        \textbf{Specificita} & 91           & 100             & 100         \\
        \hline
    \end{tabular}%
    \label{tab:rbd2}%
\end{table}%

\noindent Všechny metody vykazují velmi vysokou míru přesnosti a výsledky jsou srovnatelné.
Metoda RBDAct údajně dosahuje 100\,\% přesnosti,
nicméně výsledky nejsou plně podloženy \cite{RBDAct}.
Ze všech uvedených studií byla naše metoda testována na největším datasetu.
Pro korektní srovnání metod by bylo potřeba použít stejný dataset.

\subsection{Vyhodnocení splnění cíle 2}
Cíl \emph{Výzkum včasných digitálních biomarkerů poruch spánku
    a jejich srovnání s~jinými zdroji informací, například se spánkovými deníky či dotazníky}
byl dosažen částečně.
Byla navržena metoda identifikace poruch spánku, která dosahuje srovnatelných výsledků
s~nejnovějšími metodami v~oboru.
Nejdůležitější parametr této metody, délka probuzení WASO na základě normy, by mohla být potencionální
biomarker pro identifikaci spánkových poruch.
Pro uvedený biomarker rovněž existují normy, které byly představeny v~kapitole \ref{resin}.
Nebyl ovšem proveden pokus, který by se spoléhal pouze na tento biomarker,
metoda spoléhá na všech 26 dostupných parametrů.

Srovnání výsledků se spánkovými deníky či dotazníky rovněž nebylo prozatím provedeno,
bude provedeno po kompletaci nasbíraných dat spolupracujícím partnerem Fakultní nemocnicí u~sv. Anny.

\subsection{Navazující práce}
Na základě srovnáním s~metodami, které byly publikovány až v~době tvorby vlastního řešení, je zřejmé,
že stále existuje prostor pro zlepšení.
Velmi inspirativní se zdá být práce \cite{RBDAct}, která přináší řadu inovativních
technik a dosahuje skvělých výsledků.
Z~toho důvodu by v~ideálním případě v~rámci disertační práce mohla být
navázána spolupráce s~tímto výzkumným teamem, potencionálně spojená  se zahraniční stáží.

Metoda bude rovněž zlepšena implementací navrhovaných zlepšení metody identifikace spánku a probuzení,
s~těmito novými daty pak bude potřeba experiment zopakovat.

Po nasbíraní potřebných dat bude rovněž provedeno porovnání výsledků se spánkovými deníky a dotazníky,
k~dispozici budou data například z~dotazníku RBD-SQ (RBD Screening Questionnaire), který používá
studie \cite{Prazaci}.

\section{Identifikace onemocnění s~Lewyho tělísky na základě poruch chování v~REM spánku}
V~této oblasti bylo zatím provedeno jen prvních pár pokusů, například snaha specificky identifikovat
pacienty s~potencionální Parkinsonovou chorobou v~prodromální fázi prezentované
na konferenci EEICT 2022 \cite{EEICT2022}.
Rovněž jsou prováděny prvotní klastrové analýzy, které pomohou s~lepším pochopením dostupného datasetu.

Výsledky v~této kategorie prozatím nejsou slibné, některé studie přímo popírají možnost
rozlišit jednotlivá onemocnění pouze na základě dat aktigrafie, například studie \cite{PD_or_DLB}.
Rovněž prozatím není k~dispozici dostatečně rozměrný dataset k~realizaci úkolu.
Dataset bude rozšířen a zkompletován díky probíhající spolupráci s~Fakultní nemocnicí u~sv. Anny 
a díky spolupráci s~Fakultní nemocnicí Olomouc.

\subsection{Vyhodnocení splnění cíle 3 a 4}
Cíle \emph{Ověření senzitivity a specificity navržených biomarkerů při prodromální diagnóze onemocnění s~Lewyho tělísky}
a \emph{Klinická interpretace a průzkum škálovatelnosti navržených řešení} prozatím nebyly provedeny, z~výstupů metod
lze totiž určit pacienty s~poruchami spánku, nikoliv však konkrétně pacienty s~prodromálním onemocněním s~Lewyho tělísky.
Tyto body budou náplní dalších let doktorského studia, a budou splněny v~rámci navazující disertační práce.

\section{Harmonogram navazující práce}
Navazující práce na disertační práci a v~doktorském studiu obecně bude rozdělena po jednotlivých semestrech
a prezentována v~následujících subkapitolách.

\subsection{3. ročník, zimní semestr}
V~zimním semestru třetího ročníku budou absolvovány chybějící předměty, aby bylo dosaženo požadovaného
bodového zisku v~kategorii studijních výsledků. Bude realizována navazující práce na cíli 1, především pak
filtrace šumu a filtrace gravitačního zrychlení. Pro splnění cíle 2 budou otestována vylepšení 
identifikace spánkového okna a REM fáze spánku prezentovaná ve studii \cite{Prazaci}
a studii \cite{RBDAct}. V~ideálním případě bude navázán dialog s~teamem studie \cite{RBDAct} a dohodnuta
zahraniční stáž. Zároveň budou probíhat finální činnosti na projektu CE1581 a NU20-04-00294, kde jsou výsledky
teoretického výzkumu zároveň uplatněny v~aplikačním výzkumu. Zároveň bude vydán článek o~metodě XMTAZ
popsané v~kapitole \ref{XMTAZ} ideálně v~časopisu s~impakt faktorem.

\subsection{3. ročník, letní semestr}
V~tomto semestru bude realizována mezinárodní stáž dohodnutá v~předchozím semestru s~teamem \cite{RBDAct}.
Ve spolupráci s~tímto zahraničním partnerem budou dokončeny navazující práce cíle 2, a bude vytvořena co nejlepší
metoda identifikace RBD z~dat aktigrafie. Počítá se rovněž s~publikační činností ve spolupráci se zahraničním 
partnerem na toto téma. Zároveň budou provedeny první pokusy o~identifikaci pacientů s~prodromální
diagnózou onemocnění s~Lewyho tělísky ve skupině pacientů s~poruchou spánku RBD.
Rovněž bude probíhat spolupráce s~Fakultní nemocnicí Olomouc, kde bude realizován aplikovaný výzkum 
a budou uplatněny znalosti z~předchozích semestrů v~souvislosti s~analýzou spánkové apnoe a její léčby
na základě aktigrafických dat.

\subsection{4. ročník, zimní semestr}
V~tomto semestru budou probíhat hlavní práce na splnění cílů 3 a 4, tedy budou prováděny pokusy
o~diagnózou onemocnění s~Lewyho tělísky ve skupině pacientů s~poruchou spánku RBD a klinická interpretace výsledků.
Na této činnosti bude probíhat úzká spolupráce s~Fakultní nemocnicí u~sv. Anny, pro kterou se jedná
o~strategický výzkum. Zároveň bude pokračovat spolupráce se zahraničním partnerem.
Je plánováno vydat články s~impaktem ve spolupráci s~oběma partnery, které budou mít větší přesah i 
do oblasti zdravotnictví, díky účasti neurologických skupin a partnerů z~CEITECu. 
Zároveň se počítá s~účastí na mezinárodních konferencích na dané téma a intenzivní práci na disertační práci.
Rovněž se předpokládá dokončování aplikačního výzkumu s~Fakultní nemocnicí v~Olomouci, a společnou publikační
činností.

\subsection{4. ročník, letní semestr}
V~tomto semestru budou úspěšně dokončeny bodované aktivity, aby bylo dosaženo hranice pro obhajobu dizertační práce.
Budou probíhat finální úpravy na dizertační práci a intenzivní příprava k~doktorským zkouškám.
Vše bude zakončeno odevzdáním dizertační práce a absolvováním doktorských zkoušek. 






